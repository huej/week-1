#pragma once
class HotDog
{
public:
	HotDog();
	~HotDog();
	void CookDog(HotDog* hotdogptr);
	bool AmICooked(HotDog* dogptr);
private:
	bool Cooked = false;
};

